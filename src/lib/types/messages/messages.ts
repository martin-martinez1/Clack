
export type publicMessage = {
    id: string,
    status: string,
    lastChangeDate: string,
    message: string,
    from: string,
    sent: string,
}

