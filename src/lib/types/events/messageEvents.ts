
// Change the name to be something like messageTypeNames
export const messageHandlerNames = {
	EventNewMessage: "new_message",
	EventEditMessage: "edit_message",
	EventDeleteMessage: "delete_message",

}

export class Event {
	// Each Event needs a Type
	// The payload is not required
	type: string;
	payload: any;
	
	constructor(type:string, payload: any) {
		this.type = type;
		this.payload = payload;
	}
}

export class SendMessageEvent {
	message: string;
	from: string;

	constructor(message:string, from: string) {
		this.message = message;
		this.from = from;
	}
}

export class SendExistingMessageEvent {
	id: string;
	status: string;
	lastChangeDate: string;
	message: string;
	from: string;
	sent: string

	constructor(id:string, status: string, lastChangeDate: string, message:string, from: string, sent: string) {
		this.id = id;
		this.status = status;
		this.lastChangeDate = lastChangeDate;
		this.message = message;
		this.from = from;
		this.sent = sent;
	}
}