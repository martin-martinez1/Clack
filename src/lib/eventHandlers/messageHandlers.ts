import { messageHandlerNames } from "$lib/types/events/messageEvents";
import type { publicMessage } from "$lib/types/messages/messages";
import { get, type Writable } from "svelte/store";

interface MessageEventRoutes {
    [key: string]: (messageStore: Writable<publicMessage[]>, eventData: any) => void;
}

// Make the take the function out of the object
export const messageEventRoutes: MessageEventRoutes = {
    [messageHandlerNames.EventNewMessage]: newMessageHandler,
    [messageHandlerNames.EventEditMessage]:  editMessageHandler,
    [messageHandlerNames.EventDeleteMessage]: deleteMessageHandler,

}

function newMessageHandler(messageStore: Writable<publicMessage[]>, eventData: any) : void{
    // {from: eventData.payload.from, message: eventData.payload.message, sent: eventData.payload.sent, id: eventData.payload.id}

    messageStore.update(items => {
        items.push({status:eventData.payload.status, from: eventData.payload.from, message: eventData.payload.message, sent: eventData.payload.sent, id: eventData.payload.id, lastChangeDate: eventData.payload.lastChangeDate})
        return items
    });
}

function editMessageHandler(messageStore: Writable<publicMessage[]>, eventData: any) : void{
   let messages = get(messageStore);

   const targetId = eventData.payload.id;

   for(let i = 0; i < messages.length; i++){

        if(messages[i].id === targetId){

            messages[i] = {status:eventData.payload.status, from: eventData.payload.from, message: eventData.payload.message, sent: eventData.payload.sent, id: eventData.payload.id, lastChangeDate: eventData.payload.lastChangeDate};

            break;
        }
   }


   messageStore.set(messages);

}

function deleteMessageHandler(messageStore: Writable<publicMessage[]>, eventData: any) : void{
   let messages = get(messageStore);

   const targetId = eventData.payload.id;

   for(let i = 0; i < messages.length; i++){

        if(messages[i].id === targetId){

            messages[i] = {status:eventData.payload.status, from: eventData.payload.from, message: eventData.payload.message, sent: eventData.payload.sent, id: eventData.payload.id, lastChangeDate: eventData.payload.lastChangeDate};

            break;
        }
   }


   messageStore.set(messages);

}


