
import { get, writable } from 'svelte/store';
import { Event, SendExistingMessageEvent, SendMessageEvent } from '$lib/types/events/messageEvents';
import type { publicMessage } from '$lib/types/messages/messages';

import { messageEventRoutes } from '$lib/eventHandlers/messageHandlers';

const messageStore = writable<publicMessage[]>([]);
const connectedStore = writable<boolean>();

let conn: WebSocket;

function startWebsocket(){

	if(conn === undefined){

		conn = new WebSocket('ws://' + 'localhost:8080' + '/ws');
		
		// On Open Connection
		conn.onopen = () => {
			connectedStore.set(true);
		};
		conn.onclose = () => {
			connectedStore.set(false);
	
		};
		
		
		conn.onmessage = (e) => {
		
			const eventData = JSON.parse(e.data);
			const eventType = eventData.type

			if(messageEventRoutes[eventType] !== undefined){

				messageEventRoutes[eventType](messageStore, eventData);
			}
			
		
		};
		conn.addEventListener('user joined', (msg) => {
			console.log(msg) 
		});

	}

}

function sendMessage(messageText: string){

	const event = new Event("send_message", new SendMessageEvent(messageText, "percy"));

	conn.send(JSON.stringify(event));

}

function editMessage(editedText: string ,message: publicMessage){


	const event = new Event("edit_message", {...message, newText: editedText});

	conn.send(JSON.stringify(event));

}

function deleteMessage(message: publicMessage){

	const event = new Event("delete_message", {...message});

	conn.send(JSON.stringify(event));

}

// Exponential backoff
// Each new retry takes x^n+1 seconds: if x = 2 1, 2, 4,... 
function retryConnection(){
	if(conn && conn.readyState === WebSocket.CLOSED){
		console.log("Connection Closed")
	}
}

// on app 
function closeWebSocket(){
	conn.close();
}

export default{
	startWebsocket,
	messageSubscribe: messageStore.subscribe,
	connectedSubscribe: connectedStore.subscribe,
	sendMessage,
	editMessage,
	deleteMessage


}



